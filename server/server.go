package main

import(
	"fmt"
	"net"
	"bufio"
	"time"
	"os"
)

type client struct{
	name string
	source net.Conn
	in_stream *bufio.Reader
}

type message struct {
	origin client
	message string
	time time.Time
}

func main(){
	// Server setup
	fmt.Println("Starting a chatroom!")
	portno := ":8000"
	if len(os.Args) > 1 {
		portno = ":" + os.Args[1]
	}
	ln, err := net.Listen("tcp", portno) // Open the server
	if err != nil {
		fmt.Println(err)
		return
	}

	messages := make(chan message) // Channel for messages
	clients := make([]client, 1) // Slice of all connected clients
	server := client{name: "SERVER"} // Client object for sending server messages
	clients[0] = server

	fmt.Println("Server up! Clients can connect to port " + portno + " on this machine.")
	go broadcast(messages, &clients) 

	// Accept new clients
	fmt.Println("Now accepting new clients.")
	for {
		conn, err := ln.Accept() // Open a connection
		if err != nil {
			fmt.Println(err)
		}
		go enroll(conn, &clients, server, messages)
	}
}

func enroll(conn net.Conn, clients *[]client, server client, messages chan message) {

	// Init client object
	user := client{source: conn}
	
	// Create I/O bridge interface
	fmt.Println("New host found. Establishing input bridge...")
	user.in_stream = bufio.NewReader(user.source)

	// Get username
	fmt.Println("Input buffer initialized. Getting host's username...")
	user.source.Write([]byte("Who goes there?\n"))
	name, err := user.in_stream.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return
	}
	user.name = name[:len(name)-1]
	fmt.Println("Discovered host as " + user.name + ".")

	// Add to client list
	fmt.Println("Registering user...")
	*clients = append(*clients, user)

	// listen for messages
	go listen(user, server, messages)
}

// Listens for activity from a single client, and pours client input into a channel
func listen(user client, server client, messages chan message){

	// Announce welcome message
	messages <- message{
		origin: server,
		message: "Welcome  " + user.name + "!\n",
		time: time.Now()}
	
	// Listen for messages
	for {
		msg, err := user.in_stream.ReadString('\n') // Read until user sends a newline
		if err != nil {
			break
		}
		messages <- message{
			origin: user,
			message: msg,
			time: time.Now()}
	}

	// Announce exit message
	messages <- message{
		origin: server,
		message: user.name + " lost connection.\n",
		time: time.Now()}
}

// Broadcasts messages to all clients
func broadcast(messages chan message, clients *[]client){
	fmt.Println("Now broadcasting all messages.")
	
	for{
		msg := <- messages // Wait for data from messages channel
		t := msg.time
		t_string := fmt.Sprintf(
			"%d/%d/%d %d:%d:%d",
			t.Month(),
			t.Day(),
			t.Year(),
			t.Hour(),
			t.Minute(),
			t.Second())
		msg_string := t_string + " [" + msg.origin.name + "] > " + msg.message 

		// Broadcast to all clients
		for _, v := range *clients {
			if v.name == "SERVER"{
				fmt.Print(msg_string)
			} else if v != msg.origin{
				v.source.Write([]byte(msg_string))
			}
		}
	}
}
