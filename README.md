# Term-Com

A simple, terminal-based multi-client communication application. Initially made for my final project for Intro to Operating Systems and Concurrent Programming at Case Western Reserve University.

# Build

Clone the repo

```
git clone https://github.com/mbowcutt/term-com.git
```

Build source files

Simply run `go build` in each directory.