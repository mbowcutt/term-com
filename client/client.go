package main

import (
	"fmt"
	"net"
	"bufio"
	"os"
)

func main(){

	// Client setup
	input := bufio.NewReader(os.Stdin) // User I/O buffer
	fmt.Println("Welcome to the chat client! Please specify the address:port of the chatroom.")
	fmt.Println("Example: 127.0.0.1:8000")

	conn := connect(input) // attempt to connect, exit on success
	fmt.Println("Successfully connected to chatroom! Starting server output:")

	kill := make(chan bool) // kill channel
	go listen(conn, kill) // Listen for updates from server
	go message(input, conn) // Process user input
	<- kill // blocks until notified
}

// Prompt the user for an IP address and connect until successful
func connect(input *bufio.Reader) net.Conn {
	for{
		addr, err := input.ReadString('\n')
		conn, err := net.Dial("tcp", addr[:len(addr)-1]);
			if err == nil{
				return conn
			} else {
				fmt.Println(err)
				fmt.Println("Could not connect to that one. Try again?")
			}
	}
}

// listen for messages from server
func listen(connection net.Conn, kill chan bool){
	incomingMessages := bufio.NewReader(connection) // Server I/O buffer
	for{
		msg, err := incomingMessages.ReadString('\n') // Read until server sends newline
		if err != nil{
			break
		}
		fmt.Print(msg) // Print to user console
	}
	fmt.Println("Server closed.")
	kill <- true
}

// Process user input
func message(input *bufio.Reader, connection net.Conn){
	for{
		msg, err := input.ReadString('\n') // Read until user sends newline
		if err != nil{
			fmt.Println(err)
		}
		connection.Write([]byte(msg)) // Write data to connection
	}
}
